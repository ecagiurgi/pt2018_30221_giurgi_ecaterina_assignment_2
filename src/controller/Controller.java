package controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import controller.Controller.Console;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import modell.Queue;
import modell.StopThread;
import modell.Client;
import modell.ClientGenerator;

public class Controller {

	@FXML
	TextField nrOfQueuesTF;

	@FXML
	TextField minArrivalTimeTF;

	@FXML
	TextField maxArrivalTimeTF;

	@FXML
	TextField minServiceTimeTF;

	@FXML
	TextField maxServiceTimeTF;

	@FXML
	TextField simulationIntervalTF;

	@FXML
	TextArea logTA;

	@FXML
	VBox queueVBox;

	public void start() {
		int nrOfQueues = findIntValue(nrOfQueuesTF);
		int minArrivalTime = findIntValue(minArrivalTimeTF);
		int maxArrivalTime = findIntValue(maxArrivalTimeTF);
		int minServiceTime = findIntValue(minServiceTimeTF);
		int maxServiceTime = findIntValue(maxServiceTimeTF);
		int simulationInterval = findIntValue(simulationIntervalTF);
		
		// Init queues
		List<Queue> cozi = generateQueues(nrOfQueues);

		// Start queues
		for (Queue q : cozi) {
			q.start();
		} 

		// Start ClientGenerator
		ClientGenerator clientGenerator = new ClientGenerator(cozi, minArrivalTime, maxArrivalTime, minServiceTime,maxServiceTime);
		clientGenerator.start();
		
		// Start Display refresh
		Timeline display = new Timeline(new KeyFrame(Duration.seconds(1), e -> refreshQueues(cozi)));
		display.setCycleCount(Animation.INDEFINITE);   //are rolul unei bucle pe ciclu infinit
		display.play(); 
		
		// Start Stop Thread 
		StopThread stop = new StopThread(simulationInterval, clientGenerator);
		stop.start();
	}

	public void refreshQueues(List<Queue> queues) {

		queueVBox.getChildren().clear();
		if (!queues.isEmpty())
			for (int i = 0; i < queues.size(); i++) {
				StringBuffer sb = new StringBuffer("Queue " + (i + 1) + " : ");
				if (!queues.get(i).getClienti().isEmpty())
					for (int j = 0; j < queues.get(i).getClienti().size(); j++) {
						Client client = queues.get(i).getClienti().get(j);
						sb.append("[c" + client.getNr() + ", " + client.getRemainingTime() + "] ");
					}

				Label queueLabel = new Label(sb.toString());
				queueLabel.setLayoutX(220);
				queueLabel.setLayoutY(110 + i * 30);

				queueVBox.getChildren().add(queueLabel);
			}
	}

	private int findIntValue(TextField text) {
		return Integer.parseInt(text.getText());
	}

	public List<Queue> generateQueues(int nrOfQueues) {
		List<Queue> cozi = new ArrayList<>();
		for (int i = 1; i <= nrOfQueues; i++) {
			Queue coada = new Queue();
			cozi.add(coada);
		}
		return cozi;
	}

	public void initialize() {
		PrintStream ps = new PrintStream(new Console(logTA));
		System.setOut(ps);
	}
///////////////////////////////////////////////////////////////////Console////////////////
	public class Console extends OutputStream {
		private TextArea logTextArea;

		public Console(TextArea console) {
			this.logTextArea = console;
		}

		public void appendToTextArea(String newText) {
			Platform.runLater(() -> logTextArea.appendText(newText));
		}

		public void write(int b) throws IOException {
			appendToTextArea(String.valueOf((char) b));
		}
	}
}
