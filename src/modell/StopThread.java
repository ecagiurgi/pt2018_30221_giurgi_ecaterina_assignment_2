package modell;

public class StopThread extends Thread {
	private int simulationInterval;
	private ClientGenerator clientGenerator;

	public StopThread(int simulationInterval, ClientGenerator clientGenerator) {
		super();
		this.simulationInterval = simulationInterval;
		this.clientGenerator = clientGenerator;

	}

	public void run() {
		for (int i = simulationInterval; i > 0; i--) {
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		clientGenerator.finish();
	}

}
