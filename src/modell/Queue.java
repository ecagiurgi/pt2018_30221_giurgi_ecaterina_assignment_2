package modell;

import java.util.ArrayList;
import java.util.List;

public class Queue extends Thread {
	private volatile List<Client> clienti = new ArrayList<>();

	public void addClient(Client c) {
		clienti.add(c);
	}

	public List<Client> getClienti() {
		return clienti;
	}

	public void run() {
		while (true) {
			if (!clienti.isEmpty()) {
				Client current = clienti.get(0);
				current.start();
				try {
					current.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				clienti.remove(0);
			}
		}
	}

	public int calculateRemaningTime() {
		int suma=0;
		for(Client c: clienti) {
			suma+=c.getRemainingTime();
		}
		return suma;
		
		
	}

}
