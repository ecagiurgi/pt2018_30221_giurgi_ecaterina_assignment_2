package modell;

public class Client extends Thread {
	private int nr;
	private int serviceTime;
	private int remainingTime;
	
	
	public Client(int nr, int serviceTime) {
		super();
		this.nr = nr;
		this.serviceTime = serviceTime;
		remainingTime=serviceTime;
	}


	public void run() {
		System.out.println("Client " + nr + " starts");
		for(int i=serviceTime;i>0;i--) {
			System.out.println("Client " + nr + " remainingTime "+ remainingTime-- );
			try {
				sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Client " + nr + " finishes");
	}
	
	
	
	public int getRemainingTime() {
		return remainingTime;
	}


	public int getNr() {
		return nr;
	}


	public int getServiceTime() {
		return serviceTime;
	}

	
	

}
