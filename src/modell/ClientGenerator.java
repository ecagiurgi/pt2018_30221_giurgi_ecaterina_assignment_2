package modell;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ClientGenerator extends Thread {
	private List<Queue> cozi = new ArrayList<>();
	private Random r = new Random();
	private static int counter;
	private int maxService;
	private int minService;
	private int maxArrival;
	private int minArrival;
	private boolean running=true;

	public ClientGenerator(List<Queue> cozi) {
		super();
		this.cozi = cozi;
	}

	public ClientGenerator(List<Queue> cozi, int minArrivalTime, int maxArrivalTime, int minServiceTime,
			int maxServiceTime) {

		this.cozi = cozi;
		this.minService = minServiceTime;
		this.maxService = maxServiceTime;
		this.minArrival = minArrivalTime;
		this.maxArrival = maxArrivalTime;

	}

	public void run() {
		while (running) {
			int randomTime = r.nextInt(maxService - minService) + minService;
			Client c = new Client(counter++, randomTime);
			Queue q = QueueHelper.minQueue(cozi);
			q.addClient(c);
			try {
				int arrivalTime = r.nextInt(maxArrival - minArrival) + minArrival;
				Thread.sleep(arrivalTime * 1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}
	}
	
	public void finish() {
		running=false;

	}
	
	

}
