package modell;

import java.util.List;

public class QueueHelper {
	public static Queue minQueue(List<Queue> cozi) {
		Queue aux = null;
		for (Queue q : cozi) {
			if (aux == null || aux.calculateRemaningTime() > q.calculateRemaningTime()) {
				aux = q;
			}
		}
		return aux;
	}

}
